# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
extends GutTest

var _subject : HealthComponent
const TEST_MAX_HEALTH : float = 100.0

func before_each() -> void:
	_subject = HealthComponent.new()
	watch_signals(_subject)
	_subject.max_health = TEST_MAX_HEALTH
	set_multiplayer_authority(multiplayer.get_unique_id())
	add_child(_subject)

func after_each() -> void:
	_subject.free()

func test_that_it_has_max_health_when_ready() -> void:
	assert_eq(_subject.health, _subject.max_health)

func test_that_it_takes_damage() -> void:
	var damage_amount : float = 10
	_subject.damage(damage_amount)
	assert_eq(_subject.health, TEST_MAX_HEALTH - damage_amount)

func test_that_it_emits_health_changed_after_damage() -> void:
	_subject.damage(1)
	assert_signal_emitted(_subject, 'health_changed')

func test_that_it_emits_health_zeroed() -> void:
	_subject.damage(TEST_MAX_HEALTH)
	assert_signal_emitted(_subject, 'health_zeroed')

func test_that_it_heals_fully() -> void:
	_subject.health = 10
	_subject.heal_full()
	assert_eq(_subject.health, TEST_MAX_HEALTH)

func test_that_it_emits_health_changed_after_heal_full() -> void:
	_subject.heal_full()
	assert_signal_emitted(_subject, 'health_changed')
