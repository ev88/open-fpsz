class_name Settings extends Node

var _config_file : ConfigFile = ConfigFile.new()

var fov : int
var mouse_sensitivity : float
var inverted_y_axis : bool
var fullscreen : bool

const SETTINGS_FILE_PATH : String = "user://settings.cfg"

func _ready() -> void:
	var error : Error = _config_file.load(SETTINGS_FILE_PATH)
	if error != OK:
		print("Settings not found, using defaults")
		return

	fov = _config_file.get_value("gameplay", "fov", 90)
	mouse_sensitivity = _config_file.get_value("gameplay", "mouse_sensitivity", 0.6)
	inverted_y_axis = _config_file.get_value("gameplay", "inverted_y_axis", false)
	fullscreen = _config_file.get_value("graphics", "fullscreen", false)
	_update_screen()

func save_to_file() -> void:
	_config_file.set_value("gameplay", "fov", fov)
	_config_file.set_value("gameplay", "mouse_sensitivity", mouse_sensitivity)
	_config_file.set_value("gameplay", "inverted_y_axis", inverted_y_axis)
	_config_file.set_value("graphics", "fullscreen", fullscreen)
	_update_screen()
	_config_file.save(SETTINGS_FILE_PATH)

func _update_screen() -> void:
	if fullscreen:
		DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
	else:
		DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED)
