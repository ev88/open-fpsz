class_name Map extends Node

@export var _flagstand : Node3D
@export var _player_spawns_root : Node

var _rng : RandomNumberGenerator = RandomNumberGenerator.new()

func get_flagstand() -> Node3D:
	return _flagstand

func get_player_spawn() -> Node3D:
	var spawn_count : int = _player_spawns_root.get_child_count()
	var spawn_index : int = _rng.randi_range(0, spawn_count - 1)
	var spawn_selected : Node3D = _player_spawns_root.get_child(spawn_index)
	return spawn_selected

