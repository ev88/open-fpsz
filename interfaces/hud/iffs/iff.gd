# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
extends Control

@export var player : Player
@export var attach_point : Marker3D
@export var healthbar_low_health_color : Color = Color(1 ,0 ,0, 0.78)
@export var healthbar_mid_health_color : Color = Color(1, 1, 0, 0.78)
@export var healthbar_high_health_color : Color = Color(0, 1, 0, 0.78)
@export var iff_in_range_radius_ratio : float = 0.05

@onready var _iff_container : Control = $IFFContainer
@onready var _iff_in_range_container : Control = $IFFContainer/IFFInRangeContainer
@onready var _player_name_label : Control = $IFFContainer/IFFInRangeContainer/PlayerNameLabel
@onready var _health_bar : ProgressBar = $IFFContainer/IFFInRangeContainer/HealthBar
@onready var is_within_los : bool = false
@onready var healthbar_fill_stylebox : StyleBoxFlat = _health_bar.get_theme_stylebox("fill")

func _ready() -> void:
	_player_name_label.text = player.name

func _process(_delta : float) -> void:
	# retrieve camera for current viewport
	var camera : Camera3D = get_viewport().get_camera_3d()

	# if the player is NOT infront of the camera or camera does NOT have LOS to player
	if camera.is_position_behind(attach_point.global_position) or !is_within_los:
		_iff_container.hide() # hide the IFF and exit function
		return
	else:
		_iff_container.show() # display the IFF

func _physics_process(_delta : float) -> void:
	# https://docs.godotengine.org/en/stable/tutorials/physics/ray-casting.html
	var camera : Camera3D = get_viewport().get_camera_3d()
	is_within_los = false # always initialise trace_success as false
	var space_state : PhysicsDirectSpaceState3D = camera.get_world_3d().direct_space_state
	# do a trace check from camera to towards the player
	var query : PhysicsRayQueryParameters3D = PhysicsRayQueryParameters3D.create(camera.global_position, attach_point.global_position)
	# exclude the camera owner nodes from intersecting with the trace check
	query.exclude = [camera.owner]
	# do trace check and store result
	var result : Dictionary = space_state.intersect_ray(query)
	# if a result was returned and the hit result is the player
	if result and result["collider"] == player:
		# the player is in camera LOS
		is_within_los = true

	var new_iff_position : Vector2 = camera.unproject_position(attach_point.global_position) # get the screen location of the players head
	var viewport_size : Vector2 = get_viewport_rect().size

	# check if the unprojected point lies inside the viewport
	if !Rect2(Vector2(0, 0), viewport_size).has_point(new_iff_position):
		_iff_container.hide() # hide the IFF and exit function
		return

	# if player is NOT in range to have its healthbar and name drawn
	if (new_iff_position - viewport_size / 2).length() > iff_in_range_radius_ratio * viewport_size.length():
		_iff_in_range_container.hide() # hide the in range IFF
	else:
		_iff_in_range_container.show()

	new_iff_position.y -= _iff_container.size.y # move the IFF up so the bottom of it is at the players head
	new_iff_position.x -= _iff_container.size.x / 2  # move the IFF left so it's centered horizontally above players head

	position = new_iff_position # set the position of the IFF

func _update_health_bar(health : float) -> void:
	_health_bar.value = health
	# modify health_box stylebox depending health value
	if health < 75 and health > 40:
		healthbar_fill_stylebox.bg_color = healthbar_mid_health_color
	elif health <= 40:
		healthbar_fill_stylebox.bg_color = healthbar_low_health_color
	else:
		healthbar_fill_stylebox.bg_color = healthbar_high_health_color

func _on_health_changed(new_health : float) -> void:
	_update_health_bar(new_health)
