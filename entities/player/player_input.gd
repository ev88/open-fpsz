# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
class_name PlayerInput extends MultiplayerSynchronizer

@export var jetting : bool = false
@export var skiing : bool = false
@export var direction : Vector2 = Vector2.ZERO
@export var camera_rotation : Vector2
@export var MOUSE_SENSITIVITY : float = .6
@export var inverted_y_axis : bool = false

signal jumped
signal fired_primary
signal throwed_flag

func _ready() -> void:
	var has_authority : bool = is_multiplayer_authority()
	set_process(has_authority)
	set_process_unhandled_input(has_authority)

func _unhandled_input(event: InputEvent) -> void:
	var mouse_mode : Input.MouseMode = Input.get_mouse_mode()

	# isolate mouse events
	if event is InputEventMouseMotion:
		if mouse_mode == Input.MOUSE_MODE_CAPTURED:
			# retrieve mouse position relative to last frame
			_update_camera(event.relative)

func _update_camera(relative_motion : Vector2) -> void:
	# clamp vertical rotation (head motion)
	if inverted_y_axis:
		relative_motion.y *= -1.0
	camera_rotation.y -= relative_motion.y * MOUSE_SENSITIVITY / 100
	camera_rotation.y = clamp(camera_rotation.y, deg_to_rad(-90.0),deg_to_rad(90.0))
	# wrap horizontal rotation (to prevent accumulation)
	camera_rotation.x -= relative_motion.x * MOUSE_SENSITIVITY / 100
	camera_rotation.x = wrapf(camera_rotation.x, deg_to_rad(0.0),deg_to_rad(360.0))

func _process(_delta : float) -> void:
	direction = Input.get_vector("left", "right", "forward", "backward")
	if Input.is_action_just_pressed("jump_and_jet"):
		_jump.rpc()
	if Input.is_action_just_pressed("fire_primary"):
		_fire_primary.rpc()
	if Input.is_action_just_pressed("throw_flag"):
		_throw_flag.rpc()
	jetting = Input.is_action_pressed("jump_and_jet")
	skiing = Input.is_action_pressed("ski")

@rpc("call_local")
func _jump() -> void:
	jumped.emit()

@rpc("call_local")
func _fire_primary() -> void:
	fired_primary.emit()

@rpc("call_local")
func _throw_flag() -> void:
	throwed_flag.emit()
