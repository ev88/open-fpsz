# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
extends Node3D
class_name SpaceGun

@export var PROJECTILE : PackedScene

@onready var nozzle : Node3D = $Nozzle
@onready var inventory : Node3D = get_parent()

enum WeaponState { WEAPON_READY, WEAPON_RELOADING }

var weapon_state : WeaponState = WeaponState.WEAPON_READY

const inheritance : float = .5 # ratio
const reload_time : float = 1. # seconds

func can_fire() -> bool:
	return weapon_state == WeaponState.WEAPON_READY

func fire_primary() -> void:
	if not can_fire():
		return

	var projectile : Node = PROJECTILE.instantiate()
	projectile.transform = nozzle.global_transform
	projectile.velocity = nozzle.global_basis.z.normalized() * projectile.speed
	var inheritance_factor : float = clamp(inheritance, 0., 1.)
	projectile.velocity += (inventory.owner.linear_velocity * inheritance_factor)
	inventory.owner.add_sibling(projectile)
	var collider : ShapeCast3D = projectile.shape_cast
	collider.add_exception(inventory.owner)
	weapon_state = WeaponState.WEAPON_RELOADING
	await get_tree().create_timer(reload_time).timeout
	weapon_state = WeaponState.WEAPON_READY
