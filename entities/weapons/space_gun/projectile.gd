# This file is part of open-fpsz.
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
class_name Projectile extends Node3D

@export_category("Parameters")
@export var EXPLOSION : PackedScene
@export var speed : float = 78.4 # m/s
@export var lifespan : float = 5.0 # in seconds

@onready var shape_cast : ShapeCast3D = $ShapeCast3D
@onready var game : Node3D = get_tree().get_current_scene()

var velocity : Vector3 = Vector3.ZERO

func _ready() -> void:
	var lifespan_timer : Timer = Timer.new()
	lifespan_timer.wait_time = lifespan
	lifespan_timer.one_shot = true
	lifespan_timer.autostart = true
	lifespan_timer.timeout.connect(self_destruct)
	add_child(lifespan_timer)

func self_destruct() -> void:
	explode(position)

func explode(spawn_location : Vector3) -> void:
	var spawned_explosion : Node = EXPLOSION.instantiate()
	spawned_explosion.position = spawn_location
	game.add_child(spawned_explosion)
	queue_free()

func _physics_process(delta : float) -> void:
	var previous_position : Vector3 = global_position
	global_position += velocity * delta
	shape_cast.target_position = to_local(previous_position)
	if shape_cast.is_colliding():
		var contact_point : Vector3 = shape_cast.collision_result[0].point
		explode(contact_point)
