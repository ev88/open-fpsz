# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
extends Node3D

@onready var fire : GPUParticles3D = $Fire
@onready var explosive_damage : ExplosiveDamageComponent = $ExplosiveDamageComponent
var explosion_effect_pending : bool = false

func _ready() -> void:
	fire.emitting = true
	fire.finished.connect(func() -> void: queue_free())
