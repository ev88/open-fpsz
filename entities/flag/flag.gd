class_name Flag extends Node3D

enum FlagState { FLAG_STATE_ON_STAND, FLAG_STATE_DROPPED, FLAG_STATE_TAKEN }

@export var flag_state : FlagState = FlagState.FLAG_STATE_ON_STAND

func can_be_grabbed() -> bool:
	return flag_state != FlagState.FLAG_STATE_TAKEN

func grab() -> void:
	if flag_state != FlagState.FLAG_STATE_TAKEN:
		flag_state = FlagState.FLAG_STATE_TAKEN

func drop() -> void:
	if flag_state == FlagState.FLAG_STATE_TAKEN:
		flag_state = FlagState.FLAG_STATE_DROPPED

