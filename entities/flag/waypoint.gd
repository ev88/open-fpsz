# This file is part of open-fpsz.
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
extends Control

@export var attach_point : Node3D
@export var flag : Flag

@onready var _iff_container : Control = $IFFContainer

func _ready() -> void:
	attach_point.transform = attach_point.transform.translated(
		Vector3(0, flag.get_node("CollisionShape3D").shape.size.y, 0))

func _process(_delta : float) -> void:
	var camera : Camera3D =  get_viewport().get_camera_3d()
	var viewport_rect : Rect2 = get_viewport_rect()
	# if the player is NOT infront of the camera or camera does NOT have LOS to player
	if camera.is_position_behind(attach_point.global_position):
		_iff_container.hide() # hide the IFF and exit function
		return
	else:
		_iff_container.show() # display the IFF
	
	# get the screen location of the players head
	var new_iff_position : Vector2 = camera.unproject_position(attach_point.global_position)
	
	# check if the unprojected point lies inside the viewport
	if !Rect2(Vector2(0, 0), viewport_rect.size).has_point(new_iff_position):
		_iff_container.hide() # hide the IFF and exit function
		return
		
	# move the IFF up so the bottom of it is at the players head
	new_iff_position.y -= _iff_container.size.y
	# move the IFF left so it's centered horizontally above players head
	new_iff_position.x -= _iff_container.size.x / 2
	# set the position of the IFF
	position = new_iff_position
