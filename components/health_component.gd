# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
class_name HealthComponent extends Area3D

@export var max_health : float = 100.0
@export var health : float = 100.0:
	set(value):
		health = value
		health_changed.emit(value)

signal health_zeroed
signal health_changed(value : float)

func _ready() -> void:
	heal_full()

@rpc("call_local")
func damage(amount : float) -> void:
	health = clampf(health - amount, 0.0, max_health)
	if health == 0.0:
		health_zeroed.emit()

@rpc("call_local")
func _heal(amount : float) -> void:
	health = clampf(health + amount, 0.0, max_health)

func heal_full() -> void:
	if not is_multiplayer_authority():
		return

	_heal.rpc(max_health)
