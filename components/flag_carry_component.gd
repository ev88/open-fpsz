# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
class_name FlagCarryComponent extends Node
## This component allows its entity to grab, carry and throw flags
##
## To work correctly the owner MUST set an attachment point for carried flags
## and a sensor to detect when flags are within grab range

@export var max_throw_speed : float = 10.0
@export var attachment : Node3D
@export var sensor : Area3D

var _carried_flag : Flag

func _ready() -> void:
	sensor.body_entered.connect(_sensor_on_body_entered)

func _process(_delta : float) -> void:
	if _is_carrying():
		_carried_flag.global_position = attachment.global_position
		_carried_flag.global_rotation = attachment.global_rotation

func _grab(flag : Flag) -> void:
	if not _is_carrying():
		flag.grab()
		_carried_flag = flag

func _release(inherited_velocity : Vector3, throw_speed : float) -> void:
	if _is_carrying():
		_carried_flag.drop()
		_carried_flag.rotation_degrees.x = 0.0
		_carried_flag.linear_velocity = inherited_velocity + (attachment.global_basis.z * throw_speed)
		# Throw the flag from some distance in front of the player to avoid regrabbing mid-air
		_carried_flag.global_position = _carried_flag.global_position + (attachment.global_basis.z * 1.5)
		_carried_flag = null

func _is_carrying() -> bool:
	return _carried_flag != null

func _sensor_on_body_entered(collider : Flag) -> void:
	if collider is Flag:
		_grab(collider)

func drop() -> void:
	_release(Vector3.ZERO, 0.0)

func throw(inherited_velocity : Vector3) -> void:
	_release(inherited_velocity, max_throw_speed)
