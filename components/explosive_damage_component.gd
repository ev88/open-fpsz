# This file is part of open-fpsz.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
class_name ExplosiveDamageComponent extends Area3D

@export var damage : int = 100
@export var impulse_force : int = 1000

func _physics_process(_delta : float) -> void:
	for body in get_overlapping_bodies():
		if body is RigidBody3D:
			var center_of_mass_global_position : Vector3 = body.center_of_mass + body.global_position
			var direction : Vector3 = ( center_of_mass_global_position - global_position).normalized()
			body.apply_central_impulse(direction * impulse_force)

	for area in get_overlapping_areas():
		if area is HealthComponent and is_multiplayer_authority():
			area.damage.rpc(damage)

	set_physics_process(false)
